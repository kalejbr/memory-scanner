#/////////////////////////////////////////////////////////////////////////////////
#///           University of Hawaii, College of Engineering
#/// @brief   Lab03 - Memory Scanner - EE 491 - Spr 2022
#///
#/// @file    Makefile
#/// @version 1.0 
#///
#///  The program will open and read /proc/self/maps and parse the contents
#///
#/// @author  Kale Beever-Riordon <kalejbr@hawaii.edu>
#/// @date    08_Feb_2022
#///
#/// @see     proc man pages
#///
#////////////////////////////////////////////////////////////////////////////////

SHELL = /bin/sh
.SUFFIXES:
.SUFFIXES: .c .o

CC	= gcc
CFLAGS = -g -Wall

TARGET = maps

all: $(TARGET)

maps: main.c
	$(CC) $(CFLAGS) -o $(TARGET) main.c

test: main.c
	./maps


.PHONY: clean

clean:
	rm -f $(TARGET)
