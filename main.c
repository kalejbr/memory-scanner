/////////////////////////////////////////////////////////////////////////////////
///           University of Hawaii, College of Engineering
/// @brief   Lab03 - Memory Scanner - EE 491 - Spr 2022
///
/// @file    main.c
/// @version 1.0 - Scans each region of readable memory, returns byte count
///                 and number of 'A' characters read.
///
///  The program will open and read /proc/self/maps and parse the contents
///
/// @author  Kale Beever-Riordon <kalejbr@hawaii.edu>
/// @date    11_Feb_2022
///
/// @see     proc man pages
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define MAX_ROWS 128
#define FILE_NAME "/proc/self/maps"
#define DELIMITER " \n"

struct proc_self_Stack{
    char wholeAddress[128];
    char sStartAddress[128];
    char sEndAddress[128];
    void* startAddress;
    void* endAddress;
    char permission[128];
    char offset[128];
    char device[128];
    char inode[128];
    char path[128];
    bool readable;
};

// Print the start address, the end address, and the permissions
void printStruct ( struct proc_self_Stack stack1[]) {

        printf("0x%p - 0x%p\t[%s]\t\t", stack1->startAddress, stack1->endAddress, stack1->permission);
}

// Create void ptr 'j' and iterate from the start address to just before the end address, dereference the pointer and
// count instances of A (0x41), and count bytes.
void scanEntries ( struct proc_self_Stack stack1[], int byteCount, int ayCount){
    for( void* j = stack1->startAddress; j < (stack1->endAddress) ; j++){
        if(*(char*)j == 0x41 ) {
            ayCount++;
        }
        byteCount++;
    }
    printf("# of bytes read [%d]   \t", byteCount);
    printf("# of 'A' is [%d]\n", ayCount);
}

// Using a struct we will stream the file and parse its contents into structure members
void proc_map(){

    struct proc_self_Stack mapStruct[MAX_ROWS];
    char line[2048];
    int tokState; // the parse state machine for line
    char *tok; // for strtok_r
    char* saveptr; // for strtok_r
    int currentRow = 0;

    FILE* fp = fopen(FILE_NAME, "r");

    if (fp == NULL){
        perror("error opening this file\n");
        return;
    }

    //Fill the block of memory for our struct 'mapStruct'
    memset(mapStruct, 0, sizeof(mapStruct));

    while (fgets(line, sizeof(line), fp) != NULL) {

        tok = strtok_r(line, DELIMITER, &saveptr);
        tokState = 0; //Set state machine to the first token (address)

        while (tok != NULL){
            //...this one goes to 11.
           tokState ++;
            // the first state saves the entire address (startAddress - endAddress) to the struct
            if(tokState == 1){
                char* tok2;
                char* saveptr2;

                //The 'wholeAddress' is parsed to start and end. Stored as strings and pointers.
                strcpy(mapStruct[currentRow].wholeAddress, tok);
                tok2 = strtok_r(mapStruct[currentRow].wholeAddress, "-", &saveptr2);
                strcpy(mapStruct[currentRow].sStartAddress, tok2);
                tok2 = strtok_r(NULL, "-", &saveptr2);
                strcpy(mapStruct[currentRow].sEndAddress, tok2);
                sscanf(mapStruct[currentRow].sStartAddress, "%p", &mapStruct[currentRow].startAddress);
                sscanf(mapStruct[currentRow].sEndAddress, "%p", &mapStruct[currentRow].endAddress);

            }
            // the second state copies the permissions as a string, the string is read to determine if the file is
            // readable or not.
            if(tokState == 2){
                strcpy(mapStruct[currentRow].permission, tok);
                if(strstr(mapStruct[currentRow].permission, "r") != NULL){
                    mapStruct[currentRow].readable = true;
                }
            }
            //...and so on.
            if(tokState == 3){
                strcpy(mapStruct[currentRow].offset, tok);
            }
            if(tokState == 4){
                strcpy(mapStruct[currentRow].device, tok);
            }
            if(tokState == 5){
                strcpy(mapStruct[currentRow].inode, tok);
            }
            if(tokState == 6){
                strcpy(mapStruct[currentRow].path, tok);
            }
            tok = strtok_r(NULL, DELIMITER, &saveptr);
        }
        currentRow++;
    }

    // We now access the contents of our struct to scan and print each valid region
    for( int i = 0; i < MAX_ROWS ; i++ ) {
        int byteCount = 0;
        int ayCount = 0;
        if (mapStruct[i].readable == true && strcmp(mapStruct[i].path, "[vvar]") != 0)  {
            printStruct(&mapStruct[i]);
            scanEntries (&mapStruct[i], byteCount, ayCount);
        }
    }
    fclose(fp);
}


int main(){

    proc_map();

    return 0;
}

